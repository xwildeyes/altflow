import React from 'react'
import { ErrorMessage, Field, Form, Formik, useFormik } from 'formik'
import LaddaButton, { EXPAND_RIGHT } from 'react-ladda-button'

import 'react-ladda-button/dist/ladda.min.css'

const Login = ({
	onSendForm,
	onSuccess,
	onClickSignup,
	shouldShowOrSignup,
	// TODO timeout
	...props
}: {
	onSuccess: (result: any) => void
	onSendForm: (email: string, password: string) => Promise<any>
	shouldShowOrSignup?: boolean
	onClickSignup?: () => void
}) => {
	return (
		<Formik
			initialValues={{ email: '', password: '' }}
			validate={values => {
				const errors: any = {}
				if (!values.email) {
					errors.email = 'Required'
				} else if (
					!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
				) {
					errors.email = 'Invalid email address'
				}
				return errors
			}}
			onSubmit={async ({ email, password }, { setErrors, setSubmitting }) => {
				try {
					const result = await onSendForm(email, password)

					if (result) {
						setSubmitting(false)
						onSuccess(result)
					} else {
						setSubmitting(false)
					}
				} catch (err) {
					if (err.message?.includes('password'))
						setErrors({ password: err.message })
					if (err.message?.includes('credentials'))
						setErrors({ password: err.message })
					else console.warn(err)
					setSubmitting(false)
				}
			}}
		>
			{({ isSubmitting }) => (
				<Form translate={{}}>
					<Field type="email" name="email" placeholder="email" />
					<ErrorMessage name="email" component="div" />
					<Field type="password" name="password" placeholder="password" />
					<ErrorMessage name="password" component="div" />
					<ErrorMessage name="general" component="div" />
					<LaddaButton loading={isSubmitting} data-style={EXPAND_RIGHT}>
						Submit
					</LaddaButton>
				</Form>
			)}
		</Formik>
	)
}

export const UserAuth = {
	Login,
}
