import { observer } from 'mobx-react'
import React from 'react'
import { Link, Redirect, useHistory, useLocation } from 'react-router-dom'
import { useInject } from '../../stores/root.provider'
import { UserAuth } from './UserAuth'

export const LoginPage = observer(() => {
	const auth = useInject(rootStore => rootStore.authManager)
	const history = useHistory()
	const location = useLocation()
	const { from } = (location as any).state || { from: { pathname: '/app' } }

	if (auth.user) {
		return (
			<Redirect
				to={{
					pathname: '/app',
				}}
			/>
		)
	}

	return (
		<div>
			<h1>AltFlow</h1>
			<Link to="/register">Register instead?</Link>
			<h2>Login Form</h2>
			<UserAuth.Login
				onSendForm={(email, password) => auth.signin(email, password)}
				onSuccess={() => {
					history.replace(from)
				}}
			/>
		</div>
	)
})
