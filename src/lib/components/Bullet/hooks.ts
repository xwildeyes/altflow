import { useEffect } from 'react'
import { Node } from '../../models/Node/Node'
import { BulletFocus } from '../../types'

export function useShouldFocus(node: Node, titleRef, notesRef) {
	useEffect(() => {
		if (node.shouldFocus) {
			const ref =
				node.shouldFocus.type === BulletFocus.Title
					? titleRef.current!
					: notesRef.current!
			ref.focus()
			if (node.shouldFocus.selection) {
				const [start, end, direction] = node.shouldFocus.selection
				ref.setSelectionRange(start, end, direction)
			}
			node.shouldFocus = null
		}
	}, [node.shouldFocus])
}
