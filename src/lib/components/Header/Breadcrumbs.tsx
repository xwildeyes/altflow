import classnames from 'classnames'
import { observer } from 'mobx-react'
import React from 'react'
import { useInject } from '../../stores/root.provider'

export const Breadcrumbs = observer(() => {
	const uiManager = useInject(rootStore => rootStore.uiManager)

	return (
		<div className={classnames('Breadcrumbs')}>
			{uiManager.doc.hierarchy.map((node, index) => {
				const link = node.isHome ? '/app' : '/app/node' + node.id
				return (
					<React.Fragment key={index}>
						{index > 0 && chevronLeft}
						<a className="app__breadcrumb-title" {...link}>
							{node.title}
						</a>
					</React.Fragment>
				)
			})}
		</div>
	)
})

const chevronLeft = (
	<svg width="5" height="8" viewBox="0 0 5 8" fill="none">
		<path d="M0 0 L4 4 L0 8" stroke="currentColor" strokeLinecap="round"></path>
	</svg>
)
