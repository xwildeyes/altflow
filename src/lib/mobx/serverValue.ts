import { observable } from 'mobx'

export enum LoadingState {
	IDLE = 'IDLE',
	LOADING = 'LOADING',
	ERROR = 'ERROR',
	DONE = 'DONE',
}

class ServerValue<T> {
	@observable private _value: T
	@observable private _state: LoadingState
	constructor(defaultValue: T, _state = LoadingState.IDLE) {
		this._value = defaultValue
		this._state = _state
	}
	get value() {
		return this._value
	}
	get state() {
		return this._state
	}
	get hasValue() {
		return this._state === LoadingState.DONE
	}
	set(value, state = LoadingState.DONE) {
		this._value = value
		this._state = state
	}
}

export const serverValue = <T>(defaultValue: T, _state?: LoadingState) =>
	new ServerValue(defaultValue, _state)
