import { extractSelection } from '../../browser/keymap.helpers'
import {
	focusNotes,
	focusTitle,
	Node,
	switchPlaces,
} from '../../models/Node/Node'
import { pushBlock } from '../../../routes'
import UIManager, { animateNope } from '../ui.manager'
import { BulletFocus } from '../../types'

const bulletActions = {
	'complete-task': (node: Node) => {
		const completedValue = !node.completed
		node.completed = completedValue
			; (function recurse(b: Node) {
				b.children.forEach(bb => {
					bb.completed = completedValue
					recurse(bb)
				})
			})(node)
	},
	'toggle-focus-on-notes': (node: Node) => {
		if (node.focusedOn === BulletFocus.Notes) {
			if (!node.notes) node.notes = null
			node.shouldFocus = focusTitle
		} else {
			if (!node.notes) node.notes = ''
			node.shouldFocus = focusNotes
		}
	},
	'open-bullet-below': (node: Node) => {
		node.addNextSibling(new Node({ shouldFocus: focusTitle }))
	},
	indent: (
		node: Node,
		event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
	) => {
		event.preventDefault()
		if (node) {
			const { parentList, index } = node
			let movedNode
			if (index === 0) return animateNope()
			movedNode = parentList.splice(index, 1)[0]
			movedNode.parent = parentList[index - 1]
			parentList[index - 1].children.push(movedNode)
			movedNode.shouldFocus = {
				type: node.focusedOn,
				selection: extractSelection(event),
			}
		}
	},
	unindent: (
		node: Node,
		event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>,
		uiManager: UIManager
	) => {
		event.preventDefault()
		if (uiManager.exprtab.isBeingSuggested) return;
		const { parentList, index } = node
		let movedNode
		if (node.atRootList) return animateNope()
		movedNode = parentList.splice(index, 1)[0]
		const parent = node.parent!
		const grandpa = parent.parent
		const grandpaList = parent.parentList
		const parentId = grandpaList.indexOf(parent)
		movedNode.parent = grandpa
		grandpaList.splice(parentId + 1, 0, node)
		movedNode.shouldFocus = {
			type: node.focusedOn,
			selection: extractSelection(event),
		}
	},
	'navigate-up': (node: Node) => {
		const { previous: previousSibling } = node
		if (previousSibling) previousSibling.focus()
		else animateNope()
	},
	'navigate-down': (node: Node) => {
		const { next: nextSibling } = node
		if (nextSibling) nextSibling.focus()
		else animateNope()
	},
	backspace: (node: Node, event) => {
		if (node) {
			const { parentList, index } = node
			if (!node.title.length) {
				event.preventDefault()
				if (node.previous) {
					node.previous.focus()
					parentList.splice(index, 1)
				} else if (node.nextBrother) {
					node.nextBrother.focus()
					parentList.splice(index, 1)
				} else if (node.children.length) {
					// TBD make children[0] new parent
				} else {
					animateNope()
				}
			}
		}
	},
	'zoom-in': (node: Node) => {
		pushBlock(node)
	},
	'zoom-out': (node: Node) => {
		pushBlock(node.parent)
	},
	'move-bullet-up': (node: Node, event) => {
		const {
			previousBrother: bro,
			previous: { previousBrother: cousin },
		} = node

		if (bro) {
			switchPlaces(bro, node)
			node.restoreFocus(event)
		} else if (cousin) {
			node.selfRemove()
			cousin.addChildAtEnd(node)
			node.restoreFocus(event)
		}
	},
	'move-bullet-down': (node: Node, event) => {
		const { nextBrother: bro } = node
		const cousin = node.parent?.nextBrother
		if (bro) {
			switchPlaces(bro, node)
			node.restoreFocus(event)
		} else if (cousin) {
			node.selfRemove()
			cousin.addChildAtStart(node)
			node.restoreFocus(event)
		}
	},
	'bullet-duplicate': (node: Node, event) => {
		const duplicated = node.clone()
		node.addNextSibling(duplicated)
		duplicated.shouldFocus = {
			type: node.focusedOn,
			selection: extractSelection(event),
		}
	},
	'bullet-delete': (node: Node, event) => {
		if (node.previous) node.previous.shouldFocus = focusTitle
		node.selfRemove()
		event.preventDefault()
	},
	'bullet-collapse': (node: Node) => {
		node.open = false
	},
	'bullet-un-collapse': (node: Node) => {
		node.open = true
	},
	'expression-tab-accept-suggestion': (node: Node, event, uiManager: UIManager) => {
		event.preventDefault()

		uiManager.exprtab.acceptSuggestion(node)
	}
}

export type BulletActionName = keyof typeof bulletActions

export default bulletActions
