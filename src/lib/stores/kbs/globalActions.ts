import { Node } from '../../models/Node/Node'
import { animateNope } from '../ui.manager'
import rootStore from '../root'
import { pushBlock } from '../../../routes'
import { setScope } from './kbs.manager'

function stopDnd() {
	if (rootStore.uiManager.isDnd) rootStore.uiManager.endDnd(true)
	rootStore.uiManager.stopMultipleSelect()
	rootStore.uiManager.clearMultipleSelect()
}

const globalActions = {
	cancel: () => {
		stopDnd()
		rootStore.uiManager.isCtrlKOpen = false
	},
	'toggle-focus-on-search': (node: Node) => {
		animateNope() // TODO
	},
	'toggle-visibility-completed': (node: Node) => {
		animateNope() // TODO
	},
	'navigate-home': () => {
		pushBlock()
	},
	'ctrl-k': () => {
		setScope('global')
		rootStore.uiManager.isCtrlKOpen = !rootStore.uiManager.isCtrlKOpen
	},
}

export type GlobalActionName = keyof typeof globalActions

export default globalActions
