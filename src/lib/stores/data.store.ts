import { debounce } from 'lodash'
import { observable, when } from 'mobx'
import { deepObserve } from 'mobx-utils'
import { Node } from '../models/Node/Node'
import AuthManager from './auth.manager'
import StorageManager from './storage.manager'

export default class DataStore {
	@observable loaded = false
	@observable home: Node = new Node(
		{
			title: 'Home',
		},
		true
	)
	public nodes: { [shortid: string]: Node } = {}
	constructor(private auth: AuthManager, private storageManager: StorageManager) {
		when(() => storageManager.isDataAvailable, this.load)
	}

	load = async () => {
		const nodes = await this.storageManager.fetch()
		if (nodes) {
			this.home.acceptSubnodes(nodes)
			this.setupNodeDictionairy()
		}
		this.loaded = true

		deepObserve(
			this.home,
			debounce(({ type, object, oldValue, newValue, name }: any, path) => {
				if (type === 'update' && name === 'shouldFocus') return;
				if (this.auth.isLoggedIn) {
					// TODO verify correctness
					if (['remove', 'delete'].includes(type))
						delete this.nodes[object.id]
					if (type === 'add') this.nodes[object.id] = object
					this.storageManager.put(
						'dataStore',
						this.home.children.map(b => b.toJSON())
					)
				}
			}, 350)
		)
	}
	setupNodeDictionairy() {
		const recurse = (node: Node) => {
			this.nodes[node.id] = node

			node.children.forEach(b => recurse(b))
		}

		recurse(this.home)
	}
}
