import { flatten } from 'lodash'
import { computed, observable } from 'mobx'
import ExpressionTabManager from '../managers/expressiontab.manager'
import { Node, focusTitle } from '../models/Node/Node'
import { DROP_POS, Mouse } from '../types'
import DataStore from './data.store'
import { setScope } from './kbs/kbs.manager'

export const localstorageKey = '__altflow_ui'

export type MousePosUpdater = (mousePos: Mouse | null) => any
export type DroppingStatus = [Node, DROP_POS]

type TreeState = {
	node: Node
	top: number
	left: number
}

export default class UIManager {
	exprtab: ExpressionTabManager = new ExpressionTabManager()
	cursorIndex: number
	// settings
	@observable rtl = false
	@observable darkmode = false
	// states
	@observable private grabbing: Node | null = null
	@observable
	private updateMousePos: MousePosUpdater | null = null
	@observable private _doc: Node
	@observable
	public droppingStatus: DroppingStatus | null = null
	/** ms multiple select */
	@observable private multipleSelecteds: Node[] = []
	/** line that is currently being edited, input or textarea */
	@observable private _current: Node | null
	@observable isCtrlKOpen = false

	private multipleSelectionRoot: Node | null = null
	private dndTreeState: TreeState[] | null = null

	constructor(private dataStore: DataStore) {
		this._doc = this.dataStore.home
	}

	@computed get isDnd() {
		return Boolean(this.updateMousePos)
	}

	startDnd(grabbing: Node, updateMousePos: MousePosUpdater) {
		this.updateMousePos = updateMousePos
		this.grabbing = grabbing
		// console.log('Updating tree state for updateMousePos...')
		this.dndTreeState = (function recurse(rootline: Node): TreeState[] {
			return flatten(
				rootline.children.map(line => {
					if (grabbing === line) return []
					const {
						left,
						top,
					} = line.getInputDOMElement().getBoundingClientRect()
					return [
						{
							node: line,
							top,
							left,
						},
						...recurse(line),
					] as TreeState[]
				})
			)
		})(this.doc)
	}
	moveDnd(pos: Mouse) {
		if (this.updateMousePos && this.dndTreeState) {
			this.updateMousePos(pos)
			const treeState = this.dndTreeState
			const { x, y } = pos

			let droppingStatus: DroppingStatus | undefined
			// we set this.droppingStatus so that UI knows to show user where he's DnD-ing
			if (treeState && treeState.length) {
				for (let i = treeState.length - 1; i >= 0; i--) {
					const { node: line, top, left } = treeState[i]
					let direction: DROP_POS | undefined
					if (y > top) {
						direction = left < x ? 'CHILDREN' : 'BOTTOM'
						droppingStatus = [line, direction] as DroppingStatus
					}
					if (
						droppingStatus &&
						!(direction === 'CHILDREN' && line === this.grabbing)
					) {
						this.droppingStatus = droppingStatus
						return
					}
				}
				if (!droppingStatus) {
					const { node: line } = treeState[0]
					droppingStatus = [line, 'TOP'] as DroppingStatus
				}
			}
		}
	}
	endDnd(cancelDnd = false) {
		if (this.updateMousePos) this.updateMousePos(null)
		this.updateMousePos = null
		if (this.droppingStatus) {
			if (!this.grabbing) throw new Error('what')
			const [line, pos] = this.droppingStatus
			const notDndIntoHimself = line !== this.grabbing

			if (notDndIntoHimself && !cancelDnd) {
				this.grabbing.selfRemove()
				if (pos === 'BOTTOM') {
					line.addNextSibling(this.grabbing)
				} else if (pos === 'CHILDREN') {
					line.addChildAtStart(this.grabbing)
				} else if (pos === 'TOP') {
					line.addPreviousSibling(this.grabbing)
				}
			}
			this.droppingStatus = null
		}
		if (this.dndTreeState) this.dndTreeState = null
	}

	startMultipleSelect(line: Node) {
		this.multipleSelectionRoot = line
		// console.log('Updating tree state for startMultipleSelect...')
	}
	moveMultipleSelect({ x, y }: Mouse) {
		if (this.multipleSelectionRoot) {
			const {
				top: firstTop,
				bottom: firstBottom,
			} = this.multipleSelectionRoot
				.getInputDOMElement()
				.getBoundingClientRect()
			const draggingDownards = firstBottom < y
			const draggingUpwards = firstTop > y
			if (this.multipleSelecteds.length <= 1) {
				if (draggingDownards || draggingUpwards) {
					this.multipleSelecteds.push(this.multipleSelectionRoot)
				} else {
					this.multipleSelecteds.pop()
				}
			}
			if (this.multipleSelecteds.length >= 1) {
				const last = this.multipleSelecteds[this.multipleSelecteds.length - 1]

				if (draggingUpwards) {
					const next = last.previous
					if (next && y < getLineHalfCoordinate(next))
						this.multipleSelecteds.push(next)
					else if (
						y > getLineHalfCoordinate(last) &&
						this.multipleSelecteds.length > 1
					)
						this.multipleSelecteds.pop()
				} else if (draggingDownards) {
					const next = last.next
					if (next && y > getLineHalfCoordinate(next))
						this.multipleSelecteds.push(next)
					else if (
						y < getLineHalfCoordinate(last) &&
						this.multipleSelecteds.length > 1
					)
						this.multipleSelecteds.pop()
				}
				function getLineHalfCoordinate(line: Node) {
					const {
						top,
						height,
					} = line.getInputDOMElement().getBoundingClientRect()
					return top + height / 2 // pass this point with the mouse to select the line
				}
			}
		}
	}
	stopMultipleSelect() {
		// console.log('stopping multiple select')
		this.multipleSelectionRoot = null
	}
	clearMultipleSelect() {
		// console.log('clearing multiple select')
		for (let i = this.multipleSelecteds.length - 1; i >= 0; i--) {
			this.multipleSelecteds.pop()
		}
	}
	isMultipleSelecting(line: Node) {
		return Boolean(this.multipleSelectionRoot)
	}
	isMultipleSelected(line: Node) {
		return this.multipleSelecteds.includes(line)
	}
	setDoc(value?: Node) {
		if (!value) {
			this._doc = this.dataStore.home
			setScope('global')
		} else this._doc = value
		this.setCurrent(this._doc)
		this._doc.shouldFocus = focusTitle
	}
	get doc(): Node {
		if (this._doc) return this._doc
		else return this.dataStore.home
	}
	/* #region  KBS helpers */
	// we need these to help action know the current line

	setCurrent(line: Node) {
		this._current = line
		setScope('editing-bullet')
	}
	unsetCurrent() {
		this._current = null
		setScope('global')
	}
	get current() {
		return this._current
	}
	/* #endregion */
}

export function animateNope() {
	// console.log('TBD cute animation showing you cant do that')
}
