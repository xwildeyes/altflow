import { createContext, useContext } from 'react'
import { RootStore } from './root'

const StoreContext = createContext<RootStore>({} as RootStore)

export const useStore = () => useContext(StoreContext)
export const StoreProvider = StoreContext.Provider

export type MapStore<T> = (store: RootStore) => T

export function useInject<T>(mapStore: MapStore<T>) {
	const store = useStore()
	return mapStore(store)
}
