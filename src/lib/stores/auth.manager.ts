import { action, observable } from 'mobx';
import { useEffect } from 'react';
import { history } from '../../routes';
import { appwrite, loadAccount, signup as register } from '../integrations/appwrite/appwrite';
import { useInject } from './root.provider';

export default class AuthManager {
  @observable isLoggedIn: boolean
  @observable isLoading: boolean = true
  @observable user?: any

  @action checkAccount() {
    this.isLoading = true
    // get user when app goes up, if appwrite was logged in,
    // request will be authorized via existing cookies
    loadAccount().then(account => {
      this.user = account
      this.isLoggedIn = true
    }).catch(err => {
      // do nothing
    })
      .then(() => {
        this.isLoading = false
      })
  }

  @action async logout() {
    await appwrite.account.deleteSession('current')
    this.isLoggedIn = false
    this.user = undefined
    history.push('/login')
  }

  @action async signin(email: string, password: string) {
    await appwrite.account.createSession(email, password)
    const account = await loadAccount()

    this.user = account
    this.isLoggedIn = true

    return account
  }

  @action async register(name: string, email: string, password: string) {
    await register(name, email, password)
  }
}
