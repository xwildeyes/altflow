import { computed, observable, autorun } from 'mobx'
import shortid from 'shortid'
import { NodeJSONType, ShouldFocus, BulletFocus } from '../../types'
import { extractSelection } from '../../browser/keymap.helpers'
import SessionDataManager from '../../managers/SessionDataManager'

export class Node {
	// actual data
	public id: string
	@observable title = ''
	@observable notes: string | null = null
	@observable created: Date = new Date()
	@observable modified: Date = new Date()
	@observable starred = false
	@observable completed = false
	@observable children: Node[] = []
	private _parent?: Node = undefined
	// ui, temporary
	@observable shouldFocus: ShouldFocus
	@observable focusedOn: BulletFocus
	@observable isSelected = false
	@observable open: boolean | null = null
	public isHome = false

	constructor(props: Partial<Node> = {}, isHome = false) {
		this.id = props.id ?? shortid.generate()
		this.title = props.title ?? ''
		this.notes = props.notes ?? null
		this.created = props.created ?? new Date()
		this.modified = props.modified ?? new Date()
		this.starred = props.starred ?? false
		this.completed = props.completed ?? false
		this.children = props.children ?? []

		this._parent = props.parent ?? undefined

		this.shouldFocus = props.shouldFocus ?? null

		this.isHome = isHome

		this.open = SessionDataManager.Instance.opens[this.id]

		autorun(() => {
			SessionDataManager.Instance.opens[this.id] = this.open
			// console.log(SessionDataManager.Instance.opens[this.id])
		})
	}
	/* #region  Getters, setters, computed */
	set parent(parent: Node | undefined) {
		this._parent = parent
	}
	get index() {
		return this.parentList.indexOf(this)
	}
	get parent(): Node | undefined {
		return this._parent
	}
	get parentList() {
		if (!this.parent) throw new Error('what')
		return this.parent.children
	}
	get nextBrother() {
		return this.index + 1 < this.parentList.length
			? this.parentList[this.index + 1]
			: undefined
	}
	get previousBrother() {
		return this.index - 1 >= 0 ? this.parentList[this.index - 1] : undefined
	}
	get next() {
		const { index, children, parent, parentList } = this
		if (children.length) return children[0]
		else if (index >= parentList.length - 1) {
			if (this.atRootList) return undefined
			let currentParent = parent!
			let almostSibling = parent!.nextBrother
			while (currentParent && !almostSibling) {
				if (!currentParent.parent) return undefined
				currentParent = currentParent.parent
				almostSibling = currentParent.nextBrother
			}
			return almostSibling!
		} else {
			return parentList[index + 1]
		}
	}
	get previous() {
		const { index, parent, previousBrother: previousImmediateSibling } = this
		if (index === 0) {
			if (this.atRootList) return undefined
			return parent
		} else {
			const prev = previousImmediateSibling! // guarded by index === 0
			if (!prev.children.length) {
				return prev
			} else {
				let almostSibling = prev
				while (almostSibling.children.length) {
					let { children } = almostSibling
					almostSibling = children[children.length - 1]
				}
				return almostSibling
			}
		}
	}
	/**
	 * check if parent is the all father Line
	 */
	get atRootList() {
		return this.parent && this.parent.isHome
	}
	@computed get hierarchy(): Node[] {
		if (this.isHome) return []
		if (this.atRootList) return [this.parent!, this]
		return [...this.parent!.hierarchy, this]
	}
	get shouldDisplayNotes() {
		return this.notes !== null
	}
	/* #endregion */
	/* #region List Manipulation Methods */
	selfRemove() {
		this.parentList.splice(this.index, 1)
		this.parent = undefined
	}

	/**
	 * create and return the created child
	 */
	createChild(args?: Partial<Node>, shouldFocus?: boolean) {
		const newline = new Node(args)
		if (shouldFocus) newline.shouldFocus = { type: BulletFocus.Title }
		this.addChildAtEnd(newline)
		return newline
	}
	addChildAtEnd(...newNodes: Node[]) {
		newNodes.forEach(l => {
			l.parent = this
			this.children.push(l)
		})
	}
	addChildAtStart(...newNodes: Node[]) {
		newNodes.forEach(l => {
			this.children.unshift(l)
			l.parent = this
		})
	}
	addNextSibling(sibling: Node) {
		if (!this.parent) throw new Error('what')
		sibling.parent = this.parent
		this.parentList.splice(this.index + 1, 0, sibling)
	}
	addPreviousSibling(grabbing: Node) {
		if (!this.parent) throw new Error('what')
		grabbing.parent = this.parent
		this.parentList.splice(this.index, 0, grabbing)
	}
	clone() {
		return Node.fromJSON(JSON.parse(JSON.stringify(this)))
	}
	focus() {
		this.getInputDOMElement().focus()
	}
	getInputDOMElement() {
		return document.querySelector<HTMLInputElement>(`[data-id="${this.id}"]`)!
	}
	restoreFocus(event) {
		this.shouldFocus = {
			type: this.focusedOn,
			selection: extractSelection(event),
		}
	}
	/** splices on children, but adds parent for you */
	splice(index: number, deleteCount: number, ...lines: Node[]) {
		this.children.splice(index, deleteCount, ...lines)
		lines.forEach(node => (node.parent = this))
	}
	/**
	 * become the observable specified in the json,
	 * we load data using this method instead of replacing dataStore.home with the new data, because we cant observe dataStore.home by replacing
	 */
	acceptSubnodes(children: NodeJSONType[]) {
		this.children = children.map(d => Node.fromJSON(d, this))
	}
	/* #endregion */
	/* #region  Override built in methods */

	/**
	 * override JSON.stringify
	 */
	toJSON() {
		return {
			id: this.id,
			title: this.title,
			notes: this.notes,
			completed: this.completed,
			created: this.created,
			modified: this.modified,
			starred: this.starred,
			children: this.children.map(b => b.toJSON()),
		}
	}
	/**
	 * base method for rehydration from localstorage
	 */
	static fromJSON(lineObj: NodeJSONType, parent?: Node) {
		const obj = new Node({
			...lineObj,
			created: new Date(lineObj.created),
			modified: new Date(lineObj.modified),
			children: [],
			parent,
		})

		if (lineObj.children && lineObj.children.length)
			obj.acceptSubnodes(lineObj.children)

		return obj
	}
	toString() {
		return `Line: ${this.title}`
	}
	/* #endregion */
}

export function switchPlaces(line1: Node, line2: Node) {
	const { index: index1, parent: parent1 } = line1
	const { index: index2, parent: parent2 } = line2

	// put placeholder so the index won't change, for example
	// if we're switching between two in the same parent
	line1.parentList.splice(index1, 1, new Node())
	line2.parentList.splice(index2, 1, new Node())

	parent1.splice(index1, 1, line2)
	parent2.splice(index2, 1, line1)
}

export const focusTitle: ShouldFocus = {
	type: BulletFocus.Title,
}
export const focusNotes: ShouldFocus = {
	type: BulletFocus.Notes,
}

export const FLAG_HOME = 'FLAG_HOME'
