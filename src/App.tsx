import { observer } from 'mobx-react'
import React from 'react'
import { hot } from 'react-hot-loader/root'
import { Route } from 'react-router-dom'
import './App.scss'
import { PrivateRoute } from './lib/integrations/appwrite/PrivateRoute'
import DocumentEditorPage from './lib/pages/DocumentEditorPage'
import { LoginPage } from './lib/pages/LoginPage/LoginPage'
import { RegisterPage } from './lib/pages/LoginPage/RegisterPage'
import rootStore from './lib/stores/root'
import { StoreProvider } from './lib/stores/root.provider'

const App = observer(() => {
	return (
		<StoreProvider value={rootStore}>
			<Route path="/login">
				<LoginPage />
			</Route>
			<Route path="/register">
				<RegisterPage />
			</Route>
			<PrivateRoute path="/app">
				<DocumentEditorPage />
			</PrivateRoute>
		</StoreProvider>
	)
})

export default process.env.NODE_ENV === 'development' ? hot(App) : App
